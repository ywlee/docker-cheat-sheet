# Docker

## Docker Hub 

This is the registry which is used to host various `Docker images`.

Get Jenkins from official docker hub: 

https://www.tutorialspoint.com/docker/docker_hub.htm

## Docker Compose 

A tool for defining and running complex applications with Docker. With Compose, you define a multi-container application in a single file, then spin your application up in a single command which does everything that needs to be done to get it running.

https://docs.docker.com/compose/gettingstarted/

## Docker Image

## Docker Containers

## AWS ECS 

- Repository
- Clusters
- Task

How to push a docker image to a private repository? login, tag and push

Ref: https://stackoverflow.com/questions/28349392/how-to-push-a-docker-image-to-a-private-repository


## docker build (dockerfile to image)

```
docker build -t dockhello -f ./dockhello .
docker build -t docker-jenkins .
```

`-t`: Tag an image

`-f`: Specify a Dockerfile

## docker run (docker image to docker container)

```
docker run dockhello
docker run -p 8080:8080 -p 50000:50000 docker-jenkins
docker run -d -p 8080:8080 -p 50000:50000 docker-jenkins
docker run -i -t ubuntu-test
docker run -it --rm my-ubuntu
docker run -it --rm -p 52022:22 my-ubuntu
```

`-d`: detached mode

`-p`: Publish a container's port or a range of ports to the host

`-it`: For interactive processes (like a shell), you must use -i -t together in order to allocate a tty for the container process. -i -t is often written -it

`--rm`: delete docker container automatically after "exit"

## Use Docker to build Gitlab server and Gitlab CI

https://jigsawye.com/2015/09/25/gitlab-ce-in-docker/

http://walterinsh.github.io/2016/04/18/using-gitlab-ci.html

## Use Gitlab CI to build docker project

http://answ.me/post/build-docker-images-automatically-via-gitlab-ci/

https://docs.gitlab.com/ce/ci/docker/using_docker_build.html

http://zhaozhiming.github.io/blog/2015/11/30/gitlab-ci-runner-create-and-config/

## Network
```
docker network create --subnet=172.18.0.0/16 mynet123
docker run --net mynet123 --ip 172.18.0.15 -it --rm -p 30001:27017 my-mongo1
docker run --net mynet123 --ip 172.18.0.14 -it --rm -p 30002:27017 my-mongo2
docker run --net mynet123 --ip 172.18.0.13 -it --rm -p 30003:27017 my-mongo3
```

## Starting docker daemon on mac
```
open --background -a Docker
```

## Get access host database from a docker container is possible
```
docker.for.mac.host.internal
```
## Copy file from/into docker 
```
docker cp foo.txt {mycontainer}:/foo.txt
docker cp {mycontainer}:/foo.txt foo.txt
```
`mycontainer` is a container ID, not an image ID.
